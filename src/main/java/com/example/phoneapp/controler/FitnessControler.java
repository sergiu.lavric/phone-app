package com.example.phoneapp.controler;


import com.example.phoneapp.dto.FitnessDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.service.FitnessService;
import com.example.phoneapp.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/fitness")
public class FitnessControler {
    private FitnessService fitnessService;

    @Autowired
    public FitnessControler(FitnessService fitnessService) {
        this.fitnessService = fitnessService;
    }

    @PostMapping("/add-fitness/{uuid}")
    public ResponseEntity addFitnessApp(@RequestBody FitnessDto fitnessDto, @PathVariable UUID uuid) throws EntityNotFound {
        try {
          return   HttpStatusHelper.success ("added fitness app ", fitnessService.addFitness (fitnessDto,uuid));
        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod (e);
        }

    }
}
