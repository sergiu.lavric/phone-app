package com.example.phoneapp.controler;


import com.example.phoneapp.dto.PhoneDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.service.PhoneService;
import com.example.phoneapp.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/phone")
public class PhoneControler {
    private PhoneService phoneService;

    @Autowired
    public PhoneControler(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @PostMapping("/add-phone")
    public ResponseEntity addPhone(@RequestBody PhoneDto phoneDto) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added phone ", phoneService.addPhone (phoneDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }

    }

    @GetMapping("/get-phone/{uuid}")
    public ResponseEntity getPhone(PhoneDto phoneDto, @PathVariable UUID uuid) throws EntityNotFound {
        return HttpStatusHelper.success ("get phone", phoneService.getPhone (uuid));
    }

    @GetMapping("/get-list")
    public ResponseEntity getList() throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("get phone", phoneService.getPhoneList ());
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
    @GetMapping("/get-phone-by-brand")
    public ResponseEntity getPhoneByBrand(@RequestParam(value = "brand")String brand,PhoneDto phoneDto ) throws EntityNotFound {
        try{
            return HttpStatusHelper.success ("got phone", phoneService.getPhoneByBrand (brand ,phoneDto ));
        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
}

