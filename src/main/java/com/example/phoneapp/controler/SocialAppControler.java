package com.example.phoneapp.controler;

import com.example.phoneapp.dto.AppsDto;
import com.example.phoneapp.dto.PhoneDto;
import com.example.phoneapp.dto.SocialMediaDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.service.SocialMediaService;
import com.example.phoneapp.util.HttpStatusHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/social")
public class SocialAppControler {

    private SocialMediaService service;

    public SocialAppControler(SocialMediaService service) {
        this.service = service;
    }


    @PostMapping("/put-social/{uuid}")
    public ResponseEntity putSocialApp(@RequestBody SocialMediaDto socialMediaDto , AppsDto appsDto,   PhoneDto phoneDto,@PathVariable UUID uuid) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added social app ", service.addSocial (socialMediaDto,uuid, appsDto ));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }


}
