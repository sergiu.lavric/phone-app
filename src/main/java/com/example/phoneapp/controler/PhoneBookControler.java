package com.example.phoneapp.controler;


import com.example.phoneapp.dto.PhoneBookDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.service.PhoneBookService;
import com.example.phoneapp.util.HttpStatusHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/phone-book")
public class PhoneBookControler {

    private PhoneBookService phoneBookService;

    public PhoneBookControler(PhoneBookService phoneBookService) {
        this.phoneBookService = phoneBookService;
    }

    @PostMapping("/put-phone-book/{uuid}")
    public ResponseEntity<Object> addPhoneBook(@RequestBody PhoneBookDto phoneBookDto, @PathVariable UUID uuid) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added phone book", phoneBookService.addPhoneBook (uuid, phoneBookDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }


    }

//    @GetMapping("/get-phone-book/{uuid}")
//    public ResponseEntity <Object> getPhoneBook (@PathVariable UUID uuid,PhoneBookDto phoneBookDto) {
//        try{
//            return HttpStatusHelper.success ("get phone-book ",phoneBookService.getPhoneBook (uuid,phoneBookDto));
//
//        }catch (Exception e ) {
//            return HttpStatusHelper.commonErrorMethod (e);
//        }
//    }
}
