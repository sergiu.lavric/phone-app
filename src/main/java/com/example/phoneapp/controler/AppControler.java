package com.example.phoneapp.controler;


import com.example.phoneapp.dto.AppsDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.service.AppService;
import com.example.phoneapp.util.HttpStatusHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/apps")
public class AppControler {

    private AppService appService;

    public AppControler(AppService appService) {
        this.appService = appService;
    }

    @PostMapping("/add-app/{uuid}")
    public ResponseEntity putApps(@RequestBody AppsDto appsDto, @PathVariable UUID uuid) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added app", appService.addApps (appsDto, uuid));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }


    }


}
