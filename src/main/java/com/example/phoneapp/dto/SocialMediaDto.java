package com.example.phoneapp.dto;

import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.Phone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
public class SocialMediaDto {
    private UUID uuid;
    private String user;

    private String email;
    private Phone phone;
    private Apps apps;
}
