package com.example.phoneapp.dto;


import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.PhoneBookApp;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PhoneDto {
    private UUID uuid;
    private String brand;
    private String model;
    private PhoneBookApp phoneBookApp;
    private List<Apps> appsList;


}
