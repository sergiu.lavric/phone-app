package com.example.phoneapp.dto;

import com.example.phoneapp.model.FitnessApp;
import com.example.phoneapp.model.Phone;
import com.example.phoneapp.model.PhoneBookApp;
import com.example.phoneapp.model.SocialMediaApp;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class AppsDto {
    private UUID uuid;
    private String name;
    private Phone phone;
    private List<SocialMediaApp> socialMediaAppList;
    private List<PhoneBookApp> phoneBookAppList;
    private List <FitnessApp> fitnessAppList;


}
