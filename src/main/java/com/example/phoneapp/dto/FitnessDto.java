package com.example.phoneapp.dto;

import com.example.phoneapp.model.Apps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class FitnessDto {
    private UUID uuid;
    private String user;
    private Integer steps;
    private Apps apps;


}
