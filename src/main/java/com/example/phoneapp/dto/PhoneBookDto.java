package com.example.phoneapp.dto;


import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.Phone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PhoneBookDto {
    private UUID uuid;
    private String name;
    private Integer number;
    private Phone phone;
    private Apps apps;


}
