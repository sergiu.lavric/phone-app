package com.example.phoneapp.repository;

import com.example.phoneapp.model.Apps;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AppsRepository extends JpaRepository<Apps, UUID> {
}
