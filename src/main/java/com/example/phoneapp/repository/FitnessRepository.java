package com.example.phoneapp.repository;

import com.example.phoneapp.model.FitnessApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FitnessRepository extends JpaRepository<FitnessApp, UUID> {
}
