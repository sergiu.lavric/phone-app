package com.example.phoneapp.repository;

import com.example.phoneapp.model.SocialMediaApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SocialMediaRepository extends JpaRepository<SocialMediaApp, UUID> {
}
