package com.example.phoneapp.repository;

import com.example.phoneapp.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PhoneRepository extends JpaRepository<Phone, UUID> {
    List<Phone> findPhoneByBrand (String brand);

}
