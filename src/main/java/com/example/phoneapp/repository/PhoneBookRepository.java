package com.example.phoneapp.repository;

import com.example.phoneapp.model.PhoneBookApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PhoneBookRepository extends JpaRepository<PhoneBookApp, UUID> {
}
