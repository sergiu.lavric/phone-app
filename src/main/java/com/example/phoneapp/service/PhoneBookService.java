package com.example.phoneapp.service;

import com.example.phoneapp.dto.PhoneBookDto;
import com.example.phoneapp.exception.EntityNotFound;
import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.PhoneBookApp;
import com.example.phoneapp.repository.AppsRepository;
import com.example.phoneapp.repository.PhoneBookRepository;
import com.example.phoneapp.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class PhoneBookService {
    private final PhoneBookRepository phoneBookRepository;
    private final PhoneRepository phoneRepository;
    private AppsRepository appsRepository;

    @Autowired
    public PhoneBookService(PhoneBookRepository phoneBookRepository, PhoneRepository phoneRepository, AppsRepository appsRepository) {
        this.phoneBookRepository = phoneBookRepository;
        this.phoneRepository = phoneRepository;
        this.appsRepository = appsRepository;
    }

    public PhoneBookApp addPhoneBook(UUID uuid, PhoneBookDto phoneBookDto) throws EntityNotFound {
;         Apps apps = appsRepository.findById (uuid).orElseThrow (()->new EntityNotFoundException ("not found"));
        PhoneBookApp phoneBook = new PhoneBookApp ();

        apps.getPhoneBook ().add (phoneBook);
        phoneBook.setApps (apps);
        phoneBook.setName (phoneBookDto.getName ());
        phoneBook.setNumber (phoneBookDto.getNumber ());
        phoneBook.setUuid (phoneBookDto.getUuid ());
        phoneBookRepository.save (phoneBook);


        return phoneBook;
    }



}


