package com.example.phoneapp.service;

import com.example.phoneapp.dto.FitnessDto;
import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.FitnessApp;
import com.example.phoneapp.repository.AppsRepository;
import com.example.phoneapp.repository.FitnessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class FitnessService {

    private FitnessRepository fitnessRepository;
    private AppsRepository appsRepository;

    @Autowired
    public FitnessService(FitnessRepository fitnessRepository, AppsRepository appsRepository) {
        this.fitnessRepository = fitnessRepository;
        this.appsRepository = appsRepository;
    }

    public FitnessApp addFitness(FitnessDto fitnessDto, UUID uuid){
        Apps apps=appsRepository.findById (uuid).orElseThrow (()->new EntityNotFoundException ("not found"));
        FitnessApp fitnessApp = new FitnessApp ();
        fitnessApp.setUser (fitnessDto.getUser ());
        fitnessApp.setSteps (fitnessDto.getSteps ());
        fitnessApp.setApps (apps);
        fitnessRepository.save (fitnessApp);
        return fitnessApp;
    }
}
