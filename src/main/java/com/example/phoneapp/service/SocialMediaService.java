package com.example.phoneapp.service;

import com.example.phoneapp.dto.AppsDto;
import com.example.phoneapp.dto.SocialMediaDto;
import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.SocialMediaApp;
import com.example.phoneapp.repository.AppsRepository;
import com.example.phoneapp.repository.PhoneRepository;
import com.example.phoneapp.repository.SocialMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class SocialMediaService {

    private SocialMediaRepository socialMediaRepository;
    private PhoneRepository phoneRepository;
    private AppsRepository appsRepository;

    @Autowired
    public SocialMediaService(SocialMediaRepository socialMediaRepository, PhoneRepository phoneRepository, AppsRepository appsRepository) {
        this.socialMediaRepository = socialMediaRepository;
        this.phoneRepository = phoneRepository;
        this.appsRepository = appsRepository;
    }

    public SocialMediaApp addSocial(SocialMediaDto socialMediaDto,UUID uuid, AppsDto appsDto  ) {
        Apps app = appsRepository.findById (uuid) .orElseThrow (()->new EntityNotFoundException ("not found"));
        SocialMediaApp socialApps = new SocialMediaApp ();
        app.getSocials ().add (socialApps);
        app.setSocials (appsDto.getSocialMediaAppList ());
        socialApps.setUuid (socialMediaDto.getUuid ());
        socialApps.setEmail (socialMediaDto.getEmail ());
        socialApps.setUser (socialMediaDto.getUser ());
        socialApps.setApps (app);


        appsRepository.save (app);
        socialMediaRepository.save (socialApps);

        return socialApps;


    }


}
