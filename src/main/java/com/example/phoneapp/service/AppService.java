package com.example.phoneapp.service;


import com.example.phoneapp.dto.AppsDto;
import com.example.phoneapp.model.Apps;
import com.example.phoneapp.model.Phone;
import com.example.phoneapp.repository.AppsRepository;
import com.example.phoneapp.repository.PhoneRepository;
import com.example.phoneapp.repository.SocialMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class AppService {

    private AppsRepository appsRepository;
    private SocialMediaRepository socialMediaRepository;
    private PhoneRepository phoneRepository;


    @Autowired
    public AppService(AppsRepository appsRepository, SocialMediaRepository socialMediaRepository, PhoneRepository phoneRepository) {
        this.appsRepository = appsRepository;
        this.socialMediaRepository = socialMediaRepository;
        this.phoneRepository = phoneRepository;
    }



    public Apps addApps(AppsDto appsDto, UUID uuid) {
        Phone phone = phoneRepository.findById (uuid).orElseThrow (()->new EntityNotFoundException ("not found"));
        Apps apps = new Apps ();
      phone.getAppsList ().add (apps);
      apps.setPhone (phone);
      apps.setName (appsDto.getName ());
      apps.setSocials (appsDto.getSocialMediaAppList ());
      apps.setFitness (appsDto.getFitnessAppList ());
      apps.setPhoneBook (appsDto.getPhoneBookAppList ());
      appsRepository.save (apps);


        return apps;
    }





}
