package com.example.phoneapp.service;


import com.example.phoneapp.dto.PhoneDto;
import com.example.phoneapp.model.Phone;
import com.example.phoneapp.repository.AppsRepository;
import com.example.phoneapp.repository.PhoneRepository;
import com.example.phoneapp.repository.SocialMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PhoneService {
    private PhoneRepository phoneRepository;
    private AppsRepository appsRepository;
    private SocialMediaRepository socialMediaRepository;


    @Autowired
    public PhoneService(PhoneRepository phoneRepository, AppsRepository appsRepository, SocialMediaRepository socialMediaRepository) {
        this.phoneRepository = phoneRepository;
        this.appsRepository = appsRepository;
        this.socialMediaRepository = socialMediaRepository;
    }

    public Phone addPhone(PhoneDto phoneDto) {
        Phone phone = new Phone ();
        phone.setBrand (phoneDto.getBrand ());
        phone.setModel (phoneDto.getModel ());
        phone.setAppsList (phoneDto.getAppsList ());
        phoneRepository.save (phone);
        return phone;
    }

    public List<Phone> getPhoneList() {
        List<Phone> phoneList = phoneRepository.findAll ();
        return phoneList;
    }

    public Optional<Phone> getPhone(UUID id) {
        Optional<Phone> optionalPhone = phoneRepository.findById (id);
        return optionalPhone;

    }



// To-Do Refactor this one  to return null if dosent find anything in db , now return's an empty array of phone
    public List<Phone> getPhoneByBrand(String brand, PhoneDto phoneDto) {
       return phoneRepository.findPhoneByBrand (brand)  ;


    }
}








