package com.example.phoneapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "phone")
public class Phone {



    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @org.hibernate.annotations.Type (type ="uuid-char")
    @Column(length = 36)
    private UUID uuid;


    private String brand;

    private String model;



    @OneToMany(mappedBy = "phone", cascade = CascadeType.ALL)
    private List<Apps> appsList = new ArrayList<> ();

 }

