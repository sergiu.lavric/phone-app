package com.example.phoneapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "app")
@Entity
public class Apps {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @org.hibernate.annotations.Type (type ="uuid-char")
    @Column(length = 36)
    private UUID uuid;

    @Column(name = "name")
    private String  name;



    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "phone_id")
    private Phone phone;


    @OneToMany( mappedBy = "apps",cascade = CascadeType.ALL)

    private List<SocialMediaApp> socials = new ArrayList<> ();


    @OneToMany (mappedBy = "apps",cascade = CascadeType.ALL)
    private List <FitnessApp> fitness  =new ArrayList<> ();

    @OneToMany(mappedBy = "apps",cascade = CascadeType.ALL)
    private List <PhoneBookApp> phoneBook = new ArrayList<> ();

    public Apps(UUID uuid, String name, Phone phone, List<SocialMediaApp> socials, List<FitnessApp> fitness, List<PhoneBookApp> phoneBook) {
        this.uuid = uuid;
        this.name = name;
        this.phone = phone;
        this.socials = socials;
        this.fitness = fitness;
        this.phoneBook = phoneBook;
    }




}
